from enum import Enum, auto
from typing import List, Optional, Union, Mapping
from dataclasses import dataclass
from time import time as get_sys_time
from difflib import SequenceMatcher
import sys

@dataclass
class Loc:
    line: int
    col: int
    file_name: str


class TokenType(Enum):
    NUMBER = auto()
    PLUS=auto()
    MINUS=auto()
    MULTIPLY=auto()
    DIVIDE=auto()
    DUP=auto()
    SWAP=auto()
    DROP=auto()
    OVER=auto()
    PRINT=auto()
    IF=auto()
    WHILE=auto()
    DO=auto()
    END=auto()
    EQUALS=auto()
    NOT_EQUALS=auto()
    GREATER_THAN=auto()
    LESS_THAN=auto()
    GREATER_THAN_EQUAL=auto()
    LESS_THAN_EQUAL=auto()

assert len(TokenType) == 20, "Exhaustive handling of TokenType"
KEYWORDS: Mapping[str, TokenType] = {
    '+': TokenType.PLUS,
    '-': TokenType.MINUS,
    '*': TokenType.MULTIPLY,
    '/': TokenType.DIVIDE,
    'dup': TokenType.DUP,
    'swap': TokenType.SWAP,
    'drop': TokenType.DROP,
    'over': TokenType.OVER,
    'print': TokenType.PRINT,
    'if': TokenType.IF,
    'while': TokenType.WHILE,
    'do': TokenType.DO,
    'end': TokenType.END,
    '=': TokenType.EQUALS,
    '!=': TokenType.NOT_EQUALS,
    '>': TokenType.GREATER_THAN,
    '<': TokenType.LESS_THAN,
    '>=': TokenType.GREATER_THAN_EQUAL,
    '<=': TokenType.LESS_THAN_EQUAL,
}


@dataclass
class Token:
    loc: Loc
    value: Union[str, int]
    typ: TokenType


def get_similar_words(word: str, extra_words: List[str] = []) -> List[str]:
    builtin_words = [token_type for token_type in KEYWORDS.keys()]
    builtin_words += extra_words
    return [
        builtin_word
        for builtin_word in builtin_words
        if SequenceMatcher(None, word.lower(), builtin_word.lower()).ratio() > 0.6
    ]


def make_red(message: str) -> str:
    return '\033[91m' + message + '\033[0m'

def make_yellow(message: str) -> str:
    return '\033[93m' + message + '\033[0m'

def make_green(message: str) -> str:
    return '\033[92m' + message + '\033[0m'

def make_bold(message: str) -> str:
    return '\033[1m' + message + '\033[0m'

def show_error_in_file(loc: Loc, length: int = 1, message: str = '') -> None:
    with open(loc.file_name, 'r') as f:
        lines: List[str] = f.readlines()
    output_string: str = ''
    assert 0 <= loc.line < len(lines), f'This is a bug in the lexer. Expected in range 0 - {len(lines)}. Found {loc.line}'
    line: str = lines[loc.line]
    assert 0 <= loc.col < len(line), 'This is a bug in the lexer'
    output_string += '%s on line %d: %s\n' % (loc.file_name, loc.line + 1, make_bold(message))
    output_string += '\t%s\n' % ''.join(make_red(x) if loc.col <= i < loc.col + length else x for i, x in enumerate(line.rstrip()))
    output_string += '\t'
    output_string += ' ' * loc.col
    output_string += make_yellow('^') * min(length, len(line))
    output_string += make_bold(' HERE')
    print(output_string)

def show_error_in_file_compact(loc: Loc, length: int = 1, message: str = '') -> None:
    with open(loc.file_name, 'r') as f:
        lines: List[str] = f.readlines()
    output_string: str = ''
    assert 0 <= loc.line < len(lines), f'This is a bug in the lexer. Expected in range 0 - {len(lines)}. Found {loc.line}'
    line: str = lines[loc.line]
    assert 0 <= loc.col < len(line), 'This is a bug in the lexer'
    output_string += '%s on line %d: %s ' % (loc.file_name, loc.line + 1, make_bold(message))
    output_string += '%s' % ''.join(make_red(x) if loc.col <= i < loc.col + length else x for i, x in enumerate(line.rstrip()))
    print(output_string)

def compiler_error(loc: Loc, message: str) -> None:
    print(make_red('%s:%d:%d: %s: ' % (loc.file_name, loc.line + 1, loc.col + 1, make_bold('ERROR'))) + message, file=sys.stderr)
    
def compiler_error_show_in_file(token: Token, message: str) -> None:
    show_error_in_file(token.loc, len(str(token.value)))
    compiler_error(token.loc, message)

def compiler_note(loc: Loc, message: str) -> None:
    print(make_yellow('%s:%d:%d: %s: ' % (loc.file_name, loc.line + 1, loc.col + 1, make_bold('NOTE'))) + message)


def lex_word(loc: Loc, word: str) -> Optional[Token]:
    if len(word) == 0:
        return None
    assert len(TokenType) == 20, "Exhaustive handling of TokenType"
    if word.isdecimal():
        return Token(loc=loc, value=int(word), typ=TokenType.NUMBER)
    if word[0] == '\'':
        if len(word) == 1:
            show_error_in_file(loc)
            compiler_error(loc, 'Expected character, but found end of file.')
            exit(1)
        if len(word) == 2:
            show_error_in_file(loc, 2)
            compiler_error(loc, 'Expected end closing `\'`, but found `%s`.' % word)
            compiler_note(loc, 'Perhaps you meant: %s' % make_yellow(make_bold(word + '\'')))
            exit(1)
        if len(word) >= 3 and word[2] != '\'':
            show_error_in_file(loc, len(word))
            compiler_error(loc, 'Expected end closing `\'`, but found `%s`.' % word)
            compiler_note(loc, 'Perhaps you meant: %s' % make_yellow(make_bold(word[:2] + '\' ' + word[2:])))
            exit(1)
        char: str = word[1]
        return Token(loc=loc, value=ord(char), typ=TokenType.NUMBER)
    if word[0] == '\"':
        if word[len(word) - 1] != '\"':
            show_error_in_file(loc, len(word))
            compiler_error(loc, 'Expected closing `\"` but found `%s`' % word[len(word) - 1])
            compiler_note(loc, 'Perhaps you meant: %s' % make_yellow(make_bold(word[:-1] + '\"')))
            exit(1)
        assert False, "Strings are not implemented yet. Need to implement memory first."
    if word in KEYWORDS:
        return Token(loc=loc, value=word, typ=KEYWORDS[word])
    show_error_in_file(loc, len(word))
    compiler_error(loc, f'Unknown word `{word}`')
    for similar_word in get_similar_words(word):
        compiler_note(loc, f'Perhaps you meant `{similar_word}`')
    exit(1)


def lex_line(file_name: str, line: str, line_number: int) -> List[Token]:
    tokens: List[Token] = []
    col: int = 0
    buffer: str = ""
    loc: Loc = Loc(line_number, col, file_name)
    while col < len(line):
        character: str = line[col]
        # New word
        if character == ' ':
            if len(buffer) > 0:
                if buffer[0] != '\'' and buffer[0] != '\"':
                    token = lex_word(loc, buffer)
                    if token is not None:
                        tokens.append(token)
                        col += 1
                        buffer = ''
                        loc = Loc(line_number, col, file_name)
                        continue
            col += 1
            loc = Loc(line_number, col, file_name)
            continue
        elif character == '\\':
            loc = Loc(line_number, col, file_name)
            if col >= len(line) - 1:
                show_error_in_file(loc)
                compiler_error(loc, 'Expected backslash character, found end of line')
                exit(1)
            col += 1
            backslash_character = line[col]
            if backslash_character == 'n':
                buffer += '\n'
            elif backslash_character == 't':
                buffer += '\t'
            elif backslash_character == '\\':
                buffer += '\\'
            elif backslash_character == '\'':
                buffer += '\''
            elif backslash_character == '\"':
                buffer += '\"'
            else:
                show_error_in_file(loc, 2)
                compiler_error(loc, 'Invalid backslash character `\\%s`' % backslash_character)
                exit(1)
            character = character[:-1]
        elif character == '\'' or character == '\"':
            if len(buffer) > 0:
                if buffer[0] == character:
                    buffer += character
                    token = lex_word(loc, buffer)
                    if token is not None:
                        tokens.append(token)
                        col += 1
                        buffer = ''
                        if col < len(line) and line[col] != ' ':
                            loc = Loc(line_number, col, file_name)
                            show_error_in_file(loc, 1)
                            if character == '\'':
                                compiler_error(loc, 'Expected whitespace after character. Instead found `%s`' % make_bold(line[col]))
                            elif character == '\"':
                                compiler_error(loc, 'Expected whitespace after string definition. Instead found `%s`' % line[col])
                            else:
                                assert False, 'Unreachable'
                            compiler_note(loc, 'Perhaps you meant %s' % (make_yellow(make_bold(line[:col] + ' ' + line[col:]))))
                            exit(1)
                        col += 1
                        loc = Loc(line_number, col, file_name)
                        continue
        buffer += character
        col += 1
    if len(buffer) > 0:
        if buffer[-1] == '\n':
            buffer = buffer[:-1]
        token = lex_word(loc, buffer)
        if token is not None:
            tokens.append(token)
    return tokens


def lex_file(file_name: str) -> List[Token]:
    with open(file_name, 'r') as f:
        lines: List[str] = f.readlines()

    tokens: List[Token] = []
    for line_number in range(len(lines)):
        line: str = lines[line_number]
        line_tokens: List[Token] = lex_line(file_name, line, line_number)
        for token in line_tokens:
            tokens.append(token)
    return tokens


class OpType(Enum):
    INTRINSIC = auto()
    PUSH_INT = auto()
    IF = auto()
    WHILE = auto()
    DO = auto()
    END = auto()


@dataclass
class Op:
    token: Token
    typ: OpType
    ip: int
    value: Optional['Op']

@dataclass
class Program:
    ops: List[Op]


def parse_program(tokens: List[Token]) -> Program:
    ops: List[Op] = []
    block_stack: List[Op] = []
    ip: int = 0
    while ip < len(tokens):
        assert len(TokenType) == 20, "Exhaustive handling of TokenType"
        assert len(OpType) == 6, "Exhaustive handling of OpType"
        token: Token = tokens[ip]
        op: Op
        if token.typ == TokenType.PLUS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.MINUS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.MULTIPLY:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.DIVIDE:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.PRINT:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.DUP:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.SWAP:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.DROP:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.OVER:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.EQUALS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.NOT_EQUALS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.GREATER_THAN:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.LESS_THAN:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.GREATER_THAN_EQUAL:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.LESS_THAN_EQUAL:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.EQUALS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.NOT_EQUALS:
            op = Op(token=token, typ=OpType.INTRINSIC, ip=ip, value=None)
        elif token.typ == TokenType.NUMBER:
            op = Op(token=token, typ=OpType.PUSH_INT, ip=ip, value=None)
        elif token.typ == TokenType.IF:
            op = Op(token=token, typ=OpType.IF, ip=ip, value=None)
            block_stack.append(op)
        elif token.typ == TokenType.WHILE:
            op = Op(token=token, typ=OpType.WHILE, ip=ip, value=None)
            block_stack.append(op)
        elif token.typ == TokenType.DO:
            op = Op(token=token, typ=OpType.DO, ip=ip, value=None)
            block_stack.append(op)
        elif token.typ == TokenType.END:
            op = Op(token=token, typ=OpType.END, ip=ip, value=None)
            if len(block_stack) < 1:
                compiler_error_show_in_file(token, 'Unable to end non existent block.')
                exit(1)
            reference_op: Op = block_stack.pop()
            if reference_op.typ != OpType.DO:
                compiler_error_show_in_file(token, 'End keyword must close a do block. Received: %s' % reference_op.token.typ.name)
                show_error_in_file(reference_op.token.loc, len(str(reference_op.token.value)))
                compiler_note(reference_op.token.loc, 'Note that the start of the block is here')
                exit(1)
            
            assert reference_op.value is None, 'This is a bug in the parser.'
            reference_op.value = op
            
            if len(block_stack) < 1:
                compiler_error_show_in_file(token, 'Expected `if`, or `while` keyword before `end` keyword.')
                exit(1)
            reference_op = block_stack.pop()
            if reference_op.typ not in [OpType.IF, OpType.WHILE]:
                compiler_error_show_in_file(token, 'Expected `if`, or `while` keyword before `end`. Received: %s' % reference_op.token.typ.name)
                show_error_in_file(reference_op.token.loc, len(str(reference_op.token.value)))
                compiler_note(reference_op.token.loc, 'Note that the start of the block is here')
                exit(1)
            assert reference_op.value is None, 'This is a bug in the parser'
            reference_op.value = op
            op.value = reference_op
        else:
            assert False, f"Unreachable. This could be a bug in the lexer"
        ops.append(op)
        ip += 1
    if len(block_stack) > 0:
        for op in ops:
            compiler_error_show_in_file(op.token, 'Invalid block statement `%s`' % str(op.token.value))
            if op.value is not None:
                compiler_note(op.value.token.loc, '')
        exit(1)
    return Program(ops=ops)


class DataType(Enum):
    INT = auto()
    BOOLEAN = auto()
    

@dataclass
class DataTypeItem:
    typ: DataType
    op: Op
    operands: List['DataTypeItem']


def compiler_error_list_types_and_positions(loc: Loc, type_stack: List[DataTypeItem], expansion_message: str, depth: int = 0) -> None:
    if len(type_stack) == 0:
        return
    if depth > 2:
        return
    if len(type_stack) < 3:
        compiler_note(loc, expansion_message)
        for type_data in type_stack:
            show_error_in_file_compact(type_data.op.token.loc, len(str(type_data.op.token.value)))
            for operand in reversed(type_data.operands):
                message = 'Expanding %d operand(s) for `%s`' % (len(type_data.operands), type_data.op.token.value)
                compiler_error_list_types_and_positions(operand.op.token.loc, operand.operands, message, depth=depth + 1)


def type_check(program: Program) -> None:
    type_stack: List[DataTypeItem] = []
    block_stack: List[List[DataTypeItem]] = []
    ops: List[Op] = program.ops
    ip: int = 0
    while ip < len(ops):
        assert len(TokenType) == 20, "Exhaustive handling of TokenType"
        assert len(OpType) == 6, "Exhaustive handling of OpType"
        assert len(DataType) == 2, "Exhaustive handling of DataType"
        op: Op = ops[ip]
        if op.typ == OpType.INTRINSIC:
            if op.token.typ == TokenType.PLUS:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a.typ != b.typ:
                    compiler_error_show_in_file(op.token, f'Unable to add types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(a.typ, op, [a, b]))
            elif op.token.typ == TokenType.MINUS:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to minus types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(a.typ, op, [a, b]))
            elif op.token.typ == TokenType.MULTIPLY:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to multiply types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(a.typ, op, [a, b]))
            elif op.token.typ == TokenType.DIVIDE:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to divide types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(a.typ, op, [a, b]))
            elif op.token.typ == TokenType.DUP:
                if len(type_stack) < 1:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects one element on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                type_stack.append(DataTypeItem(a.typ, op, [a]))
                type_stack.append(DataTypeItem(a.typ, op, [a]))
            elif op.token.typ == TokenType.SWAP:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                type_stack.append(DataTypeItem(b.typ, op, [b]))
                type_stack.append(DataTypeItem(a.typ, op, [a]))
            elif op.token.typ == TokenType.DROP:
                if len(type_stack) < 1:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects one element on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
            elif op.token.typ == TokenType.OVER:
                if len(type_stack) < 3:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects one element on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                c = type_stack.pop()
                type_stack.append(DataTypeItem(b.typ, op, [b]))
                type_stack.append(DataTypeItem(c.typ, op, [c]))
                type_stack.append(DataTypeItem(a.typ, op, [a]))
            elif op.token.typ == TokenType.PRINT:
                if len(type_stack) < 1:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects one element on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
            elif op.token.typ == TokenType.EQUALS:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare equality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            elif op.token.typ == TokenType.NOT_EQUALS:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare inequality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            elif op.token.typ == TokenType.GREATER_THAN:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare inequality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            elif op.token.typ == TokenType.LESS_THAN:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare inequality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            elif op.token.typ == TokenType.GREATER_THAN_EQUAL:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare inequality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            elif op.token.typ == TokenType.LESS_THAN_EQUAL:
                if len(type_stack) < 2:
                    compiler_error_show_in_file(op.token, f'Intrinsic `{op.token.value}` expects two elements on the stack. Found {len(type_stack)} element(s)')
                    exit(1)
                a = type_stack.pop()
                b = type_stack.pop()
                if a.typ != DataType.INT and a != b:
                    compiler_error_show_in_file(op.token, f'Unable to compare inequality of types {a.typ.name} and {b.typ.name} together. These types are incompatible.')
                    exit(1)
                type_stack.append(DataTypeItem(DataType.BOOLEAN, op, [a, b]))
            else:
                assert False, 'Unreachable. This is a bug in the parser'
        elif op.typ == OpType.PUSH_INT:
            assert isinstance(op.token.value, int), 'This could be a bug in the lexer'
            type_stack.append(DataTypeItem(DataType.INT, op, []))
        elif op.typ == OpType.IF:
            assert op.value is not None and op.value.typ == OpType.END, 'This could be a bug in the parser'
        elif op.typ == OpType.WHILE:
            assert op.value is not None and op.value.typ == OpType.END, 'This could be a bug in the parser'
        elif op.typ == OpType.DO:
            assert op.value is not None, 'This is a bug in the parser'
            if len(type_stack) < 1:
                compiler_error_show_in_file(op.token, f'If statement expects one element on the stack. Found {len(type_stack)} element(s)')
                exit(1)
            a = type_stack.pop()
            if a.typ != DataType.BOOLEAN:
                compiler_error_show_in_file(op.token, '`%s` statement condition expects type `BOOLEAN`. Found type `%s` instead.' % (op.value.typ.name, a.typ.name))
                exit(1)
            block_stack.append(type_stack.copy())
        elif op.typ == OpType.END:
            assert len(block_stack) > 0, 'This is a bug in the type checker'
            assert op.value is not None and op.value.typ in [OpType.IF, OpType.WHILE], 'This is a bug in the parser'
            pre_do_block = block_stack.pop()
            get_diff_index: int = 0
            while get_diff_index < min(len(pre_do_block), len(type_stack)) and \
                    pre_do_block[get_diff_index] == type_stack[get_diff_index]:
                get_diff_index += 1
            diff_pre_do: List[DataTypeItem] = pre_do_block[get_diff_index:]
            diff_cur_block: List[DataTypeItem] = type_stack[get_diff_index:]
            if len(diff_pre_do) != len(diff_cur_block):
                if len(diff_pre_do) > 0:
                    compiler_error_list_types_and_positions(op.token.loc, diff_cur_block, 'Stack expansion on difference: Pre do block')
                if len(diff_cur_block) > 0:
                    compiler_error_list_types_and_positions(op.token.loc, diff_cur_block, 'Stack expansion on difference: Post do block')
                compiler_error_show_in_file(op.token, 'Found discrepancy in block.')
                compiler_note(op.value.token.loc, 'Pre do block stack types: [ %s ]' % make_bold(', '.join(typ.typ.name for typ in pre_do_block)))
                compiler_note(op.token.loc, 'Post do block stack types: [ %s ]' % make_bold(', '.join(typ.typ.name for typ in type_stack)))
                exit(1)
        else:
            assert False, 'Unreachable. This is a bug in the parser'
        ip += 1
    assert len(block_stack) == 0, 'This is a bug in the parser'
    if len(type_stack) > 0:
        compiler_error(ops[-1].token.loc, 'Unhandled data on the stack (%s elements). Types: %s' % 
                       (make_bold(str(len(type_stack))), make_bold(', '.join(typ.typ.name for typ in type_stack))))
        compiler_error_list_types_and_positions(ops[-1].token.loc, type_stack, 'Giving expansion of unhandled data sources')
        exit(1)


def simulate(program: Program) -> None:
    stack: List[int] = []
    ops: List[Op] = program.ops
    ip: int = 0
    while ip < len(ops):
        assert len(TokenType) == 20, "Exhaustive handling of TokenType"
        assert len(OpType) == 6, "Exhaustive handling of OpType"
        op: Op = ops[ip]
        if op.typ == OpType.INTRINSIC:
            if op.token.typ == TokenType.PLUS:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(b + a)
            elif op.token.typ == TokenType.MINUS:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(b - a)
            elif op.token.typ == TokenType.MULTIPLY:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(b * a)
            elif op.token.typ == TokenType.DIVIDE:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(b // a)
            elif op.token.typ == TokenType.DUP:
                assert len(stack) >= 1, 'This is a bug in the type checker'
                a = stack.pop()
                stack.append(a)
                stack.append(a)
            elif op.token.typ == TokenType.SWAP:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(b)
                stack.append(a)
            elif op.token.typ == TokenType.DROP:
                assert len(stack) >= 1, 'This is a bug in the type checker'
                a = stack.pop()
            elif op.token.typ == TokenType.OVER:
                assert len(stack) >= 3, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                c = stack.pop()
                stack.append(a)
                stack.append(b)
                stack.append(c)
            elif op.token.typ == TokenType.PRINT:
                assert len(stack) >= 1, 'This is a bug in the type checker'
                a = stack.pop()
                print(a, end='')
            elif op.token.typ == TokenType.EQUALS:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b == a))
            elif op.token.typ == TokenType.NOT_EQUALS:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b != a))
            elif op.token.typ == TokenType.GREATER_THAN:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b > a))
            elif op.token.typ == TokenType.LESS_THAN:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b < a))
            elif op.token.typ == TokenType.GREATER_THAN_EQUAL:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b >= a))
            elif op.token.typ == TokenType.LESS_THAN_EQUAL:
                assert len(stack) >= 2, 'This is a bug in the type checker'
                a = stack.pop()
                b = stack.pop()
                stack.append(int(b <= a))
            else:
                assert False, 'Unreachable. This is a bug in the parser'
        elif op.typ == OpType.PUSH_INT:
            assert isinstance(op.token.value, int), 'This could be a bug in the lexer'
            stack.append(op.token.value)
        elif op.typ == OpType.IF:
            pass
        elif op.typ == OpType.WHILE:
            pass
        elif op.typ == OpType.DO:
            assert len(stack) >= 1, 'This is a bug in the type checker'
            assert op.value is not None and op.value.typ == OpType.END, 'This is a bug in the parser'
            condition = stack.pop()
            if condition != 1:
                jump_op = op.value
                assert jump_op.value is not None and op.value == jump_op, 'This is a bug in the parser'
                # Will be incremented at end of loop, so minus 1
                ip = jump_op.ip
            
        elif op.typ == OpType.END:
            assert op.value is not None and op.value.typ in [OpType.IF, OpType.WHILE], 'This could be a bug in the parser'
            assert len(OpType) == 6, 'Exhaustive handling of OpType when closing END'
            if op.value.typ == OpType.IF:
                pass
            elif op.value.typ == OpType.WHILE:
                jump_op = op.value
                assert jump_op.value is not None and op.value == jump_op, 'This is a bug in the parser'
                # Will be incremented at end of loop, so minus 1
                ip = jump_op.ip - 1
        else:
            assert False, 'Unreachable. This is a bug in the parser'
        ip += 1
    
    
def compile(program: Program) -> None:
    assert False, 'Not implemented'


def usage() -> None:
    print("""
Usage: orth.py 
        <filename>  : The file to compile
        -sim        : Simulate the program (interpreter)
        -com        : Compile (compiler)
        --unsafe    : Disable type checker (not recommended)
    """)

if __name__ == '__main__':
    argc: int = len(sys.argv)
    if argc < 2:
        print(make_red("Expected at least one argument: Filename"))
        usage()
        exit(1)
    file_name: str = sys.argv[1]
    
    if argc < 3:
        print(make_red('Expected at least two arguments. Please specify simulation or compilation mode.'))
        usage()
        exit(1)
    
    simulate_flag: bool = False
    compile_flag: bool = False
    unsafe_flag: bool = False
    for arg in sys.argv[2:]:
        assert len(arg) != 0, f'This shouldn\'t happen: `{arg}`'
        if arg.startswith('-'):
            if arg == '-sim':
                if compile_flag:
                    print(make_red('Cannot both simulate and compile. Both flags -sim, and -com were supplied.'))
                    exit(1)
                simulate_flag = True
            if arg == '-com':
                if simulate_flag:
                    print(make_red('Cannot both simulate and compile. Both flags -sim, and -com were supplied.'))
                    exit(1)
                compile_flag = True
            if arg == '--unsafe':
                unsafe_flag = True
    
    started_time = get_sys_time()
    tokens: List[Token] = lex_file(file_name)
    program: Program = parse_program(tokens)
    if not unsafe_flag:
        type_check(program)
    if simulate_flag:
        simulate(program)
    if compile_flag:
        compile(program)
        finished_time = get_sys_time()
        compile_time = (finished_time - started_time) * 1000
        print(make_green('Successfully compiled in %.2fms' % compile_time))

